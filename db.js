var mysql = require("mysql");
var request = require("request");
var distance = require("./distance.js");
var async = require("async");
var LineBot = require('line-bot-sdk');
var date = new Date();
var client = LineBot.client({
    channelID: '1464181271',
    channelSecret: '5252b97ed171e197e6c83b41dff4080c',
    channelMID: 'uac45417720c7de5dfe727ea6b8d55643'
});
//建立資料庫連線
function Connect(){
  var connection  = mysql.createConnection({
      host: 'db.mis.kuas.edu.tw',
      user: 's1103137225',
      password: 'zxaecdqxz',
      database: 's1103137225',
  });
  connection.connect();
  return connection;
}

function SQL(sql){ //處理部分SQL語法
    var connection = Connect();
    connection.query(sql,function(err){
       if(err) throw err;
    });
    connection.end();
}

function Searchlocation(area){ //尋找使用者所居住之行政區之所有垃圾車
    var connection = Connect();
    var latlng = new Array;
    var i =0;
    var sql = "Select lat,lng From taoyuan_garbage where area = '"+area+"'";
    connection.query(sql,function(err,result){
        if(err) throw err;
        result.forEach(function(row){
            latlng[i] = new Array;
            latlng[i][0] = row.lat;
            latlng[i][1] = row.lng;
            i++;
        });
    });
    connection.end();
    return latlng;
}

function FirstSearch(mid,useraddr){ //儲存使用者住處經緯度
    var URLuseraddr =encodeURI(useraddr);
    request("http://maps.googleapis.com/maps/api/geocode/json?address="+URLuseraddr+"", function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var result = JSON.parse(body);
            var userlat = result.results[0].geometry.location.lat;
            var userlng = result.results[0].geometry.location.lng;
            SQL("Update taoyuan_garbage_user Set lat ='"+userlat+"',lng='"+userlng+"' Where mid='"+mid+"'");
        }
    });
}

function Distance(latlng,mid){ //尋找使用者住處最近的垃圾車地點
    var GetDistance = new Array;
    var userlatlng = new Array;
    var Shortlat,Shortlng;
    var shortdistance=99999;
    async.series({
        one: function(callback){
          var connection = Connect();
            setTimeout(function(){
                callback(null,0);
            },1000);
        },
        two: function(callback){
            userlatlng = SearchUser("Select lat,lng From taoyuan_garbage_user where mid='"+mid+"'");
            setTimeout(function(){
                callback(null,1);
            },500);
        },
        three:function(callback){
            for(var i=0;i<latlng.length;i++){
                GetDistance[i] = new Array;
                GetDistance[i][0] = distance.getDistance(userlatlng[0],userlatlng[1],latlng[i][0],latlng[i][1]);
                GetDistance[i][1] = latlng[i][0];
                GetDistance[i][2] = latlng[i][1];
            }
            callback(null,2);
        },
        four:function(callback){
            for(var i=0;i<GetDistance.length;i++){
                if(shortdistance>=GetDistance[i][0]){
                    shortdistance = GetDistance[i][0];
                    Shortlat = GetDistance[i][1];
                    Shortlng = GetDistance[i][2];
                }
            }
            callback(null,3);
        },
        five:function(callback){
            Res("Select address,Monday,TuesdaytoSaturday From taoyuan_garbage where lat='"+Shortlat+"' AND lng='"+Shortlng+"'",mid,shortdistance);
            callback(null,4);
        }
    },function(err,result){
        if(err) throw err;
    });
}

function Time(sql){ //定時查詢
    var time = new Date;
    var connection = Connect();
    var Hour = (time.getHours()+8) , Minute = (time.getMinutes())+5 , week = new Date().getDay();
    if(Minute>=60){
        Minute = Minute - 60;
        Hour = Hour + 1;
    }
    if(Hour >= 24){
        Hour = Hour - 24;
    }
    
    
    if(week==1){
        sql="Select mid,address,Monday as time "+sql+" Monday like '";
    }else if(week>=2 && week<=6){
        sql="Select mid,address,TuesdaytoSaturday as time "+sql+" TuesdaytoSaturday like '";
    }
    
    
    
    if(Hour<10){
        sql=sql+"0"+Hour+":";
    }else{
        sql=sql+Hour+":";
    }
    if(Minute<10){
        sql=sql+"0"+Minute+"'";
    }else{
        sql=sql+Minute+"'";
    }
    
    
    connection.query(sql,function(err,result){
        if(err) throw err;
        result.forEach(function(row){
            if(row.address == null || row.time==null) return;
            client.sendText(row.mid,"垃圾車將於五分鐘後("+row.time+")到達\n"+row.address);
        });
    });
}

function Res(sql,mid,shortdistance){ //回傳最近垃圾車地點
    var connection = Connect();
    connection.query(sql,function(err,result){
        if(err) throw err;
        result.forEach(function(row){
            SQL("Update taoyuan_garbage_user Set garbage_address='"+row.address+"',Monday='"+row.Monday+"',TuesdaytoSaturday='"+row.TuesdaytoSaturday+"' where mid ='"+mid+"'");
            var sql = "最近的垃圾車地點："+row.address+"\n距離約 "+parseInt(shortdistance)+" 公尺";
            if(row.Monday!="" && row.TuesdaytoSaturday!=""){
                sql=sql+"\n週一垃圾車時間："+row.Monday;
                sql=sql+"\n週二到週六垃圾車時間："+row.TuesdaytoSaturday;
            }else if(row.Monday!="" && row.TuesdaytoSaturday==""){
                sql=sql+"\n週一垃圾車時間："+row.Monday;
                sql=sql+"\n週二到週六以及週日不收垃圾";
            }else if(row.Monday=="" && row.TuesdaytoSaturday!=""){
                sql=sql+"\n週二到週六垃圾車時間："+row.TuesdaytoSaturday;
                sql=sql+"\n週一以及週日不收垃圾";
            }
            client.sendText(mid,sql);
        });
    });
    connection.end();
}

function SearchUser(sql,area){ //取得使用者住址的經緯度
    var connection = Connect();
    var result =[];
    connection.query(sql,function(err,data){
        if(err) throw err;
        data.forEach(function(row){
            result[0] = row.lat;
            result[1] = row.lng;
        });
    });
    connection.end();
    return result;
}

//搜尋最近的垃圾車資訊 並更新資料庫 (該函式未匯出)
function SearchVicinity(sql){
    var connection = Connect();
    var result = [];
    connection.query(sql,function(err,data){
       if(err) throw err;
       data.forEach(function(row){
          result[0] = row.addr;
          result[1] = row.timeone;
          result[2] = row.timetwo;
       });
    });
    connection.end();
    return result;
}

function administrator(text,mid){
    var connection = Connect();
    connection.query("Select admin From taoyuan_garbage_user where mid='"+mid+"'",function(err,result){
       if(err) throw err;
       result.forEach(function(row){
           console.log(text);
           if(row.admin == "是"){
                connection.query("Select mid From taoyuan_garbage_user where id >= 1",function(err,result){
                    if(err) throw err;
                    result.forEach(function(row){
                        console.log(row.mid);
                        client.sendText(row.mid,text);
                    });
                });
           } 
       });
    });
}

exports.Connect=Connect;
exports.SQL = SQL;
exports.Searchlocation = Searchlocation;
exports.FirstSearch = FirstSearch;
exports.Distance=Distance;
exports.Time=Time;
exports.administrator = administrator;
