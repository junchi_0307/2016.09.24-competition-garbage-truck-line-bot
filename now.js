var mysql = require("mysql");
var request = require("request");
var distance = require("./distance.js");
var async = require("async");
var LineBot = require('line-bot-sdk');
var client = LineBot.client({
    channelID: '1464181271',
    channelSecret: '5252b97ed171e197e6c83b41dff4080c',
    channelMID: 'uac45417720c7de5dfe727ea6b8d55643'
});
//建立資料庫連線
function Connect(){
  var connection  = mysql.createConnection({
      host: 'db.mis.kuas.edu.tw',
      user: 's1103137225',
      password: 'zxaecdqxz',
      database: 's1103137225',
  });
  connection.connect();
  return connection;
}
var date = new Date();
var week;

if(date.getHours()+8>=24){
    week=date.getDay()+1;
}else{
    week=date.getDay();
}


function nowSearch(userlat,userlng,area,mid){
    (userlat+" "+userlng+" "+area+" "+mid);
    var connection = Connect();
    var GetDistance = new Array;
    var i=0;
    if(week==1){
        connection.query("Select lat,lng From taoyuan_garbage where area='"+area+"' and Monday !=''",function(err,result){
            if(err) throw err;
                result.forEach(function(row){
                GetDistance[i] = new Array;
                GetDistance[i][0] = distance.getDistance(userlat,userlng,row.lat,row.lng);
                GetDistance[i][1] = row.lat;
                GetDistance[i][2] = row.lng;
                i++;
            });
        });
    }else if(week>=2 && week<=6){
        connection.query("Select lat,lng From taoyuan_garbage where area='"+area+"' and TuesdaytoSaturday !=''",function(err,result){
            if(err) throw err;
                result.forEach(function(row){
                GetDistance[i] = new Array;
                GetDistance[i][0] = distance.getDistance(userlat,userlng,row.lat,row.lng);
                GetDistance[i][1] = row.lat;
                GetDistance[i][2] = row.lng;
                i++;
            });
        });
    }else{
        client.sendText(mid,"今日停止行駛垃圾車");
        return;
    }
    
    setTimeout(function(){
        compare(GetDistance,mid);
    },1000);
}

function Nowlatlng(mid,useraddr,area){ //儲存使用者輸入地之經緯度
    var URLuseraddr =encodeURI(useraddr);
    request("http://maps.googleapis.com/maps/api/geocode/json?address="+URLuseraddr+"", function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var result = JSON.parse(body);
            var userlat = result.results[0].geometry.location.lat;
            var userlng = result.results[0].geometry.location.lng;
            nowSearch(userlat,userlng,area,mid);
        }
    });
}


function Nowgarbagelatlng(mid,useraddr,area){ //儲存使用者輸入地之經緯度
    var URLuseraddr =encodeURI(useraddr);
    request("http://maps.googleapis.com/maps/api/geocode/json?address="+URLuseraddr+"", function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var result = JSON.parse(body);
            var userlat = result.results[0].geometry.location.lat;
            var userlng = result.results[0].geometry.location.lng;
            Nowgarbage(mid,area,userlat,userlng);
        }
    });
}


function Nowgarbage(mid,area,userlat,userlng){
    //var Shortlat,Shortlng;
    var time = new Date;
    var connection = Connect();
    //var GetDistance = new Array;
    var Hour = (time.getHours()+8) , Minute = (time.getMinutes()), week = new Date().getDay();
    var sql;
    var YN=true;
    //var ia = 0;
    for(var i=0;i<=20;i++){
        Minute = Minute + 1;
        if(Minute>=60){
            Minute = Minute - 60;
            Hour = Hour + 1;
        }
        if(Hour >= 24){
            Hour = Hour - 24;
        }
        if(week == 1){
            sql="select Monday as time,address,lat,lng from taoyuan_garbage where area = '"+area+"' and Monday = '";
        }else if(week >= 2 && week <= 6){
            sql="select TuesdaytoSaturday as time,address,lat,lng from taoyuan_garbage where area = '"+area+"' and TuesdaytoSaturday = '";
        }
        if(Hour<10){
            sql = sql + "0" + Hour;
        }else{
            sql = sql + Hour;
        }
        if(Minute<10){
            sql = sql + ":0" + Minute+"'";
        }else{
            sql = sql + ":" + Minute+"'";
        }
        
        connection.query(sql,function(err,result){
        if(err) throw err;
            result.forEach(function(row){
                var dis = parseInt(distance.getDistance(userlat,userlng,row.lat,row.lng));
                
                setTimeout(function(){
                    if(dis<2000){
                        var text = "垃圾車時間："+row.time+"\n附近的垃圾車地點："+row.address+"\n距離約 "+dis+" 公尺";
                            client.sendText(mid,text);
                        YN=false;
                    }
                },500);
            });
        });
    }
    setTimeout(function(){
        if(YN){
            client.sendText(mid,"20分鐘內無任何垃圾車出現附近兩公里內");
        }
    },2000);
}







function compare(GetDistance,mid){
    var Shortlat,Shortlng;
    var shortdistance=99999;
    var text;
    async.series({
        one: function(callback){
            for(var i=0;i<GetDistance.length;i++){
                if(shortdistance>=GetDistance[i][0]){
                    shortdistance = GetDistance[i][0];
                    Shortlat = GetDistance[i][1];
                    Shortlng = GetDistance[i][2];
                }
            }
            callback(null,1)
        },
        two: function(callback){
            var connection = Connect();
            connection.query("Select address,Monday,TuesdaytoSaturday from taoyuan_garbage where lat = '"+Shortlat+"' AND lng = '"+Shortlng+"'",function(err,result){
               if(err) throw err;
               result.forEach(function(row){
                    text = "最近地點："+row.address+"\n距離約 "+parseInt(shortdistance)+" 公尺";
                    if(row.Monday != ""){
                        text = text + "\n週一垃圾車時間："+row.Monday;
                    }
                    if(row.TuesdaytoSaturday != ""){
                        text = text + "\n週二到週六垃圾車時間："+row.TuesdaytoSaturday;
                    }
               });
            });
            setTimeout(function(){
                callback(null,2);
            },500);
        },
        three: function(callback){
            client.sendText(mid,text);
            callback(null,3);
        }
    },function(err,result){
        if(err) throw err
    });
}

exports.Nowlatlng = Nowlatlng;
exports.Nowgarbagelatlng = Nowgarbagelatlng;
